const data = require('./../dataBaseStructure/test.json')

var appRouter = function (app) {
  app.get("/", function (req, res) {
    res.status(200).send({ message: 'Welcome :D'})
  });

  app.get("/users", function (req, res) {
    res.status(200).send(data)
  });
  app.get("/user/id/:num", function (req, res) {
    let num = req.params.num
    if ( num <= data.length-1) {
      res.status(200).send(data[num])
    } else {
      res.status(404).send({ error: `There's not such user with id: ${num}`})
    }
  });

  app.get("/user/id/:num/friends", function (req, res) {
    let num2 = req.params.num
    if ( num2 <= data.length-1) {
      res.status(200).send(data[num2].friends)
    } else {
      res.status(404).send({ error: `There's not such user with id: ${num2}`})
    }
  });
}

//
//
// LEARRNING REFERENCE 
//
//
//
//   app.get("/", function (req, res) {
//     res.status(200).send({ message: 'Welcome to our restful API' });
//   });

//   app.get("/user", function (req, res) {
//     var data = ({
//       firstName: faker.name.firstName(),
//       lastName: faker.name.lastName(),
//       username: faker.internet.userName(),
//       email: faker.internet.email()
//     });
//     res.status(200).send(data);
//   });

//  app.get("/users/:num", function (req, res) {
//    var users = [];
//    var num = req.params.num;

//    if (isFinite(num) && num  > 0 ) {
//      for (i = 0; i <= num-1; i++) {
//        users.push({
//            firstName: faker.name.firstName(),
//            lastName: faker.name.lastName(),
//            username: faker.internet.userName(),
//            email: faker.internet.email()
//         });
//      }

//      res.status(200).send(users);
    
//    } else {
//      res.status(400).send({ message: 'invalid number supplied' });
//    }

//  });
// }

module.exports = appRouter;